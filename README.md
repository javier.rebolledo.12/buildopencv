# buildOpenCV
> Jetson Nano Developer Kit por default trae una versión de OpenCV pero existen varios motivos para re-instalar OpenCV *from source*. 

**En nuestro caso necesitamos:**
* CUDA support
* OpenCV version 4.x.x

---
### Checking
- Vamos a revisar la versión que se tiene y su configuración actual: 

> En el terminal:
```shell
$ python
$ import cv2 
$ print(cv2.__version__)
$ print(cv2.getBuildInformation() )
```
![](info.gif)

> if: `version 3.x.x` or `Other third-party libraries: Use Cuda: NO`, then continue.

> else: versión > `4.x.x` and `Use Cuda: YES` then jump to [Testing](#Testing)

### Clone

- Clone this repo: `git clone https://gitlab.com/javier.rebolledo.12/buildopencv.git`
- cd buildOpenCV

### Building

- Existen varios recursos Online para instalar OpenCV en la Jetson, nosotros utilizaremos el script de JetsonHacks. 

Building for:
* Jetson Nano
* L4T 32.2.1/JetPack 4.2.2
* OpenCV 4.1.1

<em><b>Nota: </b> Este script no revisa que versión de L4T se tiene antes de instalar. Por lo que podría no funcionar en versiones más recientes (ej. 32.4.2), pero demás que si!. </em> (last check on Dec 10, 2019)

> this is a long build, go have a coffe... or two (tiempo aprox 2.5 hrs) 
```shell
$ ./buildOpenCV.sh |& tee openCV_build.log
```

- Si bien la compilación aún se escribirá en la consola, el registro de compilación se escribirá en openCV_build.log para su posterior revisión.

- una vez terminado puedes hacer un [checking](#Checking), cerciorando los cambios.

---

### Testing

> Colocar un cronómetro en frente de la cámara y un péndulo o reloj análogo (algo que tenga movimiento periódico)

- go to `$ cd examples`

- run `$ python rtsp_test.py -url rtsp://xxx.xxx.xx.xx/example`





