import cv2
import numpy as np
import time

height = 693
width = 870
element = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3), (1, 1))
f = 0
fr = 0
mpt = 0
old_candidates = []
ad = 0 

cap = cv2.VideoCapture("rtsp://192.168.1.40/screenlive")

while(True):
	f +=1
	flag = False
	start_time = time.time()
	_, frame = cap.read()
	frame = cv2.resize(frame,(1280,720)) 
	frame2 = frame[22:715, 230:1100]
	lab= cv2.cvtColor(frame, cv2.COLOR_BGR2LAB)
	l, _, _ = cv2.split(lab)
	th2 = cv2.adaptiveThreshold(l,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,237,0)
	erosion_dst = cv2.erode(th2[22:715, 230:1100], element)

	numberOfWPixels =cv2.countNonZero(erosion_dst)
	numberOfBPixels = height*width -numberOfWPixels

	if numberOfBPixels/numberOfWPixels >1: 
		#print("fondo oscuro")
		_, labels, stats, _ = cv2.connectedComponentsWithStats(erosion_dst , 4 , cv2.CV_32S)
	else:
		#print("fondo claro")
		erosion_dst = cv2.bitwise_not(erosion_dst)
		_, labels, stats, _ = cv2.connectedComponentsWithStats(erosion_dst , 4 , cv2.CV_32S)

	largest_index = 0
	largtest_stat = 0

	mask = np.zeros([height, width], dtype=np.uint8)

	for i, stat in enumerate(stats):
		if (stat[4]>largtest_stat)&(i!=0): 
			largest_index = i
			largtest_stat = stat[4]
	
	mask[labels == largest_index] = 255
	mask = cv2.dilate(mask,element, iterations=3)
	mask2 = cv2.bitwise_not(mask)

	_, _, stats2,_ = cv2.connectedComponentsWithStats(mask2 , 4 , cv2.CV_32S)

	areas = []
	tats = []

	for i in range(len(stats2)):
		if (i!=0):
			if(stats2[i][4]>250):
				x,y = stats2[i][0],stats2[i][1]
				w,h = stats2[i][2],stats2[i][3]
				if (int(x+w)<width-3)&(int(y+h)<height-3)&(w<width*3/4)&(h<height*3/4):
					areas.append(stats2[i][4])
					tats.append(stats2[i])

	mean = np.mean(areas) 
	sigma = np.std(areas)
	temp = np.zeros([height, width,3], dtype=np.uint8)

	new_candidates = [] 

	for i, ar in enumerate(areas):
		if(ar > (mean+(sigma*3))):
			ad += 1 
			x,y = tats[i][0],tats[i][1]
			w,h = tats[i][2],tats[i][3]
			cX,cY = int((x+x+w)/2), int((y+y+h)/2)
			cv2.circle(frame2, (cX,cY), 3, (0,0,255), cv2.FILLED, 4)
			new_candidates.append([cX,cY,x,y,0,ad])

	if old_candidates:
		for coords in old_candidates: 
			cx2,cy2 = coords[0],coords[1]
			alpha = coords[4]
			for co in new_candidates:
				x,y = co[2],co[3]
				dist = np.sqrt((cx2-co[0])**2+(cy2-co[1])**2)
				if (alpha < 3)&(dist<10):
					coords[4] += 1
					co[4] = coords[4]
					co[5] = coords[5]
					if (co[4] == 3):
						cv2.rectangle(temp, (int(x),int(y)), ((x+2*(co[0]-x)), y+2*(co[1]-y)), (0,255,0), cv2.FILLED, 4)
						cv2.putText(frame2, " id%s"%co[5], (co[0],co[1]),cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2, 8)
						flag = True
				if(alpha>=3)&(dist<50):
					coords[4] += 1
					co[4] = coords[4]
					co[5] = coords[5]
					cv2.rectangle(temp, (int(x),int(y)), ((x+2*(co[0]-x)), y+2*(co[1]-y)), (0,255,0), cv2.FILLED, 4)
					cv2.putText(frame2, " id%s"%co[5], (co[0],co[1]),cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2, 8)
					flag = True

	old_candidates = new_candidates

	final = cv2.addWeighted(temp, 0.2, frame2, 1-0.2, 0)

	fr += (time.time() - start_time)
	mpt = round(fr/f, 3)
	cv2.putText(final, "fame: %s"%f, (360,680),cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0), 2, 8)

	#if(flag==True):
		#cv2.imwrite("./data/test_detection/frame%s.jpg"%f, final)
		#cv2.imwrite("./data/test_detection/Mask_frame%s.jpg"%f, mask2)

	cv2.imshow("image", final)
	
	#print(f)
	#cv2.imshow("mask2", mask2)

	key = cv2.waitKey(1) & 0xFF
	if  key == ord('q'):
		break
	if key == ord('p'):
		cv2.waitKey(-1)

cap.release()
cv2.destroyAllWindows()