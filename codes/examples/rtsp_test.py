import cv2
import numpy as np
import os



def main(args):

	path = args.rtsp_url

	if (args.trans == 'udp'):
		os.environ["OPENCV_FFMPEG_CAPTURE_OPTIONS"] = "rtsp_transport;udp"

	cap = cv2.VideoCapture(path,cv2.CAP_FFMPEG)
	
	while(True):
		ret , frame = cap.read() 
		if not ret: 
			print("error capturing frame")

		frame = cv2.resize(frame,(1280,720))
		lab= cv2.cvtColor(frame, cv2.COLOR_BGR2LAB)
		l, _, _ = cv2.split(lab)
		th2 = cv2.adaptiveThreshold(l,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,237,0) 
		cv2.imshow("video", th2)

		key = cv2.waitKey(1) & 0xFF
		if  key == ord('q'):
			break
	
	cap.release()
	cv2.destroyAllWindows()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="script for testing OpenCV and rtsp video capture")

    parser.add_argument("-url", dest="rtsp_url",
        help="rtsp_url: ejemplo: rtsp://192.168.1.40/screenlive")
    parser.add_argument("-transport", dest="trans",
    	help="transport_protocol: udp or tcp")
    args = parser.parse_args()

    main(args)